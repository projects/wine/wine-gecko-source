%define geckodir %_datadir/wine/gecko
Name: wine-gecko
Version: 1.0.0
Release: alt1

Summary: Custom version of Mozilla's Gecko Layout Engine for Wine

License: MPL
Group: Office
Url: http://wiki.winehq.org/Gecko

Packager: Vitaly Lipatov <lav@altlinux.ru>

Source: http://prdownloads.sourceforge.net/wine/wine_gecko-%version-x86.cab

BuildArchitectures: noarch

%description
Wine implements its own version of Internet Explorer. The implementation
is based on a custom version of Mozilla's Gecko Layout Engine.
When your application tries to display a web page, it loads Wine's
custom Gecko from the file wine_gecko-1.0.0-x86.cab 
Wine looks for this file first in /usr/share/wine/gecko/
Wine 1.1.33 and later will download Gecko when a prefix is created.
Older Wines will download Gecko when an application tries to display a web page.

%install
mkdir -p %buildroot%geckodir
install -m 644 %SOURCE0 %buildroot%geckodir

%files
%dir %_datadir/wine/
%dir %geckodir/
%geckodir/wine_gecko-%version-x86.cab

%changelog
* Sun Dec 06 2009 Vitaly Lipatov <lav@altlinux.ru> 1.0.0-alt1
- initial build for ALT Linux Sisyphus

